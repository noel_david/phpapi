<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<h3><a href="https://www.gyrocode.com/articles/jquery-datatables-load-more-button/" target="_blank">jQuery DataTables: "Load more" button</a> <small>Client-side processing</small></h3>

<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Office</th>
            <th>Extn.</th>
            <th>Start date</th>
            <th>Salary</th>
        </tr>
    </thead>
</table>

<div class="dt-more-container">
   <button id="btn-example-load-more" style="display:none">Load More</button>
</div>

<hr><a href="https://www.gyrocode.com/articles/tag/jquery-datatables/" target="_blank">See more articles about jQuery DataTables</a> on <a href="https://www.gyrocode.com/articles/" target="_blank">Gyrocode.com</a>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
$(document).ready(function (){
   var table = $('#example').DataTable({
      dom: 'frt',
      ajax: 'https://api.myjson.com/bins/qgcu',
      drawCallback: function(){
         // If there is some more data
         if($('#btn-example-load-more').is(':visible')){
            // Scroll to the "Load more" button
            $('html, body').animate({
               scrollTop: $('#btn-example-load-more').offset().top
            }, 1000);
         }

         // Show or hide "Load more" button based on whether there is more data available
         $('#btn-example-load-more').toggle(this.api().page.hasMore());
      }      
   });
 
   // Handle click on "Load more" button
   $('#btn-example-load-more').on('click', function(){  
      // Load more data
      table.page.loadMore();
   });
});
</script>
